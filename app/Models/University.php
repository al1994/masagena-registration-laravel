<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class University extends Model
{
    use HasUuids;

    protected $table = 'university';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'slugId',
        'name',
        'country',
        'province',
        'faculty',
        'majors',
        'description',
        'sites',
        'image',
        'author',
        'status',
        'isDelete',
    ];

    protected $casts = [
        // 'name' => 'array',
        'faculty' => 'array',
        'majors' => 'array',
    ];

    public function universityMajor() {
        return $this->hasMany('App\Models\UniversityMajor');
    }

    public function index() {
        return University::where('status', 'active')->select('id', 'slugId', 'name', 'country', 'faculty', 'majors')->where('isDelete', 'false')->get();
    }

    public function store($data) {
        University::create($data);
    }
}
