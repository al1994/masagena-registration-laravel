<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionsProfile extends Model
{
    protected $table = 'options';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'optionsWeb',
        'optionsContact',
        'emailText',
        'agreement',
        'registrationTag',
        'registrationTagActive',
    ];

    protected $casts = [
        'optionsWeb' => 'array',
        'optionsContact' => 'array',
        'registrationTag' => 'array',
    ];

    public function getSetting() {
        return OptionsProfile::first();
    }
}
