<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class UniversityMajor extends Model
{
    use HasUuids, HasFactory;

    protected $table = 'university_major';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'university_id',
        'level',
        'slugFaculty',
        'faculty',
        'slugMajor',
        'major',
        'spp',
        'author'
    ];

    public function university()
    {
        return $this->belongsTo('App\Models\University');
    }
}
