<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class RegistrationProfile extends Model
{
    use HasUuids;

    protected $table = 'registration_profile';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $fillable = [
        'userId',
        'registrationNumber',
        'registrationTag',
        'name',
        'birthPlace',
        'birthDate',
        'gender',
        'maritalStatus',
        'citizen',
        'passportNumber',
        'photo',
        'address',
        'regency',
        'province',
        'posCode',
        'phone',
        'whatsapp',
        'fatherName',
        'motherName',
        'academicBackground',
        'institutionName',
        'institutionCountry',
        'graduationDate',
        'graduationScore',
        'statusResume',
        'statusReceipt',
        'statusScholarship',
        'statusTypeScholarship',
        'statusConfirmRegistration',
        'statusConfirmFile',
        'universityListRegister',
        'universityListPass',
        'file'
    ];

    protected $casts = [
        'file' => 'array',
    ];

    public function getProfile($id) {

        return RegistrationProfile::where('userId', $id)->select(
                    'name',
                    'birthPlace',
                    'birthDate',
                    'gender',
                    'maritalStatus',
                    'citizen',
                    'passportNumber',
                    'address',
                    'regency',
                    'province',
                    'posCode',
                    'phone',
                    'whatsapp'
                )->first();
    }

    public function updateProfile($id, $data) {

        return RegistrationProfile::where('userId', $id)->update($data);
    }

    public function getParent($id) {

        return RegistrationProfile::where('userId', $id)->first(['fatherName', 'motherName', 'file']);
    }
}
