<?php

namespace App\Http\Controllers\Registration;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\RegistrationProfile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Registration\StoreAccount;
use App\Http\Controllers\Services\ImageService;
use App\Http\Controllers\Services\Encryption;

class UserController extends Controller
{
    private $image_service;
    private $encryption;

    public function __construct(ImageService $imageService, Encryption $encryption) {
        $this->image_service = $imageService;
        $this->encryption = $encryption;
    }

    public function store(StoreAccount $request) {

        try {

            $isSuccessUploaded = $this->image_service->handleUploadedImage($request->file('receipt'), 'receipt');

            if($isSuccessUploaded) {

                $data = DB::transaction(function() use ($request) {

                    $user = User::find(auth('sanctum')->user()->id);
                    $user->destination = $request->destination;
                    $user->level = $request->level;
                    $user->typeRegistration = $request->typeRegistration;
                    $user->accountStatus = "waiting";
                    $user->save();

                    RegistrationProfile::create([
                        'userId' => auth('sanctum')->user()->id,
                        'file' => (object) array('receipt' => 'receipt.jpg')
                    ]);

                    return $user;
                });

                return response()->json([
                    'success' => true,
                    'data' => $data,
                ], 200);
            }

            return response()->json([
                'success' => false,
                'message' => 'Bukti Transfer gagal diunggah.'
            ], 500);

        } catch (\Throwable $th) {

            return response()->json([
                'success' => 'Throwable',
                'message' => $th->getMessage(),
                'line' => $th->getLine()
            ], 500);

        } catch (\Illuminate\Database\QueryException $e) {

            return response()->json([
                'success' => 'QueryException',
                'message' => $e->getMessage(),
                'line' => $e->getLine()
            ], 500);
        }
    }

    public function activate(Request $request, $id, $username) {

        try {

            $user = User::where('id', $id)->where('username', $username)->first();

            if($user) {
                $user->accountStatus = 'active';
                $user->save();
                header("location: https://komiku.com/the-legendary-moonlight-sculptor-chapter-1/");
                die();
            }
            
            return response()->json([
                'success' => false,
                'message' => "Aktivasi gagal. Data tidak ditemukan"
            ], 404);

        } catch (\Illuminate\Database\QueryException $e) {
            // Illuminate\Contracts\Encryption\DecryptException
            return response()->json([
                'success' => 'QueryException',
                'message' => $e->getMessage(),
                'line' => $e->getLine()
            ], 500);

        }
    }
}
