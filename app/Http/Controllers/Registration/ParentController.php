<?php

namespace App\Http\Controllers\Registration;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\University;
use App\Models\RegistrationProfile;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\ImageService;
use App\Http\Requests\Registration\StoreParent;

use App\Models\OptionsProfile;


class ParentController extends Controller
{
    private $id;
    private $profile;
    private $image_service;

    public function __construct(ImageService $imageService, RegistrationProfile $profile, University $univ) {
        $this->profile = $profile;
        $this->image_service = $imageService;
        $this->univ = $univ;
        // $this->id = auth('sanctum')->user()->id;
        $this->id = 1;

    }

    public function index() {

        $data = $this->profile->getParent($this->id);

        if(!$data->fatherName == null) {
            return response()->json([
                'success' => true,
                'data' => $data,
            ], 200);
        }

        return response()->json([
            'success' => true,
            'message' => $data->fatherName,
        ], 404);
    }

    public function store(StoreParent $request) {

        try {
            
            DB::beginTransaction();

            $fatherIdentity = "";
            $fileName = [];
            $dataProfile = [
                'fatherName' => $request->fatherName,
                'motherName' => $request->motherName,
            ];

            if($request->has("fatherIdentity")) {
                $fileName["fatherIdentity"] = "fatherIdentity.jpg";
                $dataProfile['file->fatherIdentity'] = "fatherIdentity.jpg";
                $fatherIdentity = $this->image_service->handleUploadedImage($request->file('fatherIdentity'), $fileName["fatherIdentity"]);
            }

            if($request->has("motherIdentity")) {
                $fileName["motherIdentity"] = "motherIdentity.jpg";
                $dataProfile['file->motherIdentity'] =  "motherIdentity.jpg";
                $motherIdentity = $this->image_service->handleUploadedImage($request->file('motherIdentity'), $fileName["motherIdentity"]);
            }
            
            if($fatherIdentity || $motherIdentity) {
                
                $this->profile->updateProfile($this->id, $dataProfile);                
                
                DB::commit();
                return response()->json([
                    'success' => true,
                    'data' => array_merge($dataProfile, $fileName)
                ], 200);
            }

            return response()->json([
                'success' => false,
                'message' => 'Gambar gagal diunggah.'
            ], 500);

        } catch (\Throwable $th) {

            DB::rollBack();
            return response()->json([
                'success' => 'Throwablee',
                'message' => $th->getMessage(),
                'line' => $th->getLine()
            ], 500);

        } catch (\Illuminate\Database\QueryException $e) {

            DB::rollBack();
            return response()->json([
                'success' => 'QueryException',
                'message' => $e->getMessage(),
                'line' => $e->getLine()
            ], 500);
        }
    }

    public function testing() {

        $res = DB::table('university')->select('id', 'majors')->where('faculty', '!=' ,null)->get();

        foreach ($res as $key => $value) {
            dd($res[$key]);
            $faculty = [];
            $majors = [];
            $array = [];

            $aa = json_decode($res[$key]->majors);
            foreach ($aa as $key2 => $value2) {
                $array[$key2] = [
                    "slugId" => $aa[$key2]->slugId,
                    "faculty" => $aa[$key2]->faculty,
                    "level" => $aa[$key2]->level,
                ];
                    
                $bb = $aa[$key2]->major;
                foreach ($bb as $key3 => $value3) {

                    $array[$key2]['major'][$key3]['slugId'] = \Str::slug($bb[$key3]->name);
                    $array[$key2]['major'][$key3]['name'] = trim($bb[$key3]->name);
                    if($bb[$key3]->spp == "") {
                        $array[$key2]['major'][$key3]['spp'] = ""; 
                    } else {
                        $str = trim($bb[$key3]->spp);
                        $spp = substr(trim($str), 2);
                        // dd($str);
                        $array[$key2]['major'][$key3]['spp'] = "IDR ".str_replace(",",".",$spp); 
                    }
                }
            }
            University::where('id', $res[$key]->id)->update(['majors' => $array]);
        }
        // dd($array);
        dd('selesai');
    }

    public function objectMajors($level, $fac, $data) {
        $array = [];
        $array['slugId'] = \Str::slug($fac);
        $array['faculty'] = $fac;
        $array['level'] = $level;

        foreach ($data as $key => $value) {
            $res = explode('Spp', $value);
            $array['major'][$key]['name'] = trim($res[0]);
            $array['major'][$key]['spp'] = $res[1] ?? ""; 
        }
        // dd($array);
        return $array;

    }

    public function objectFaculty($level, $faculty) {
        $fac = [
            'slugId' => \Str::slug($faculty),
            'faculty' => $faculty,
            'level' => $level
        ];

    return $fac;
    }
}