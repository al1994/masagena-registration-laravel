<?php

namespace App\Http\Controllers\Registration;

use App\Models\RegistrationProfile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Registration\StoreProfile;
use Illuminate\Http\Request;


class RegistrationProfileController extends Controller
{
    private $id;
    private $profile;

    public function __construct(RegistrationProfile $profile) {
        $this->profile = $profile;
        $this->id = auth('sanctum')->user()->id;
    }

    public function index() {

        $data = $this->profile->getProfile($this->id);

        if($data) {
            return response()->json([
                'success' => true,
                'data' => $data,
            ], 200);
        }

        return response()->json([
            'success' => true,
            'message' => "Data Not Found",
        ], 404);
    }

    public function store(StoreProfile $request) {

        $data =  $request->all();
        $newData = [
            'registrationNumber' => '001',
            'registrationTag' => 'ME001',
        ];

        return response()->json([
            'success' => true,
            'message' => $this->profile->storeProfile($this->id, array_merge($data, $newData)),
        ], 404);
    }
}
