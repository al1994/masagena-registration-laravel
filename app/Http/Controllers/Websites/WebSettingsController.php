<?php

namespace App\Http\Controllers\Websites;

use App\Http\Controllers\Controller;
use App\Models\OptionsProfile;
use Illuminate\Http\Request;

class WebSettingsController extends Controller
{
    public function setting() {

        try {

            $webProfile = new OptionsProfile();

            return response()->json([
                'success' => true,
                'data' => $webProfile->getSetting()->select('optionsWeb')->first()
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'data' => "Terjadi Kesalahan Server",
                'message' => $th->getMessage()
            ], 500);
        }


    }
}
