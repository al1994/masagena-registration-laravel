<?php

namespace App\Http\Controllers\Websites;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\University;

class UniversityController extends Controller
{
    public function __construct(University $univ) {
        $this->univ = $univ;
    }

    public function index() {

        return response()->json([
            'success' => true,
            'data' => $this->univ->index()
        ], 200);
    }
}
