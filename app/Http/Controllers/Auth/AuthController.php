<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Services\RegNumberGenerate;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\Auth\StoreRegister;
use App\Http\Requests\Auth\StoreLogin;


class AuthController extends Controller
{
    public function __construct(RegNumberGenerate $regNumber) {
        $this->regNumber = $regNumber;
    }

    public function register(StoreRegister $request){

        try {

            $res = $this->regNumber->generate();

            $user = [
                'registrationNumber' => $res['number'],
                'registrationTag' => $res['tag'],
                'username' => $request->username,
                'email' => $request->email,
                'level' => 's1',
                'typeRegistration' => 'Reguler',
                'accountStatus' => 'register'
            ];
            
            $newCreate = array_merge($user, ['password' => Hash::make($request->password)]);

            $user = User::create($newCreate);
            $token = $user->createToken('ApiToken')->plainTextToken;

            return response()->json([
                'success' => true,
                'data' => $user,
                'token' => $token
            ]);

        } catch (\Throwable $th) {

            return response()->json([
                'status' => 'Throwable',
                'message' => $th->getMessage(),
                'line' => $th->getLine()
            ], 500);

        } catch (\Illuminate\Database\QueryException $e) {

            return response()->json([
                'status' => 'QueryException',
                'message' => $e->getMessage(),
                'line' => $e->getLine()
            ], 500);
        }


    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(StoreLogin $request)
    {
        $user = User::where('email', $request->user)->orWhere('username', $request->user)->first();
        
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response([
                'success'   => false,
                'message' => ['These credentials do not match our records.']
            ], 404);
        }
    
        $token = $user->createToken('ApiToken')->plainTextToken;
    
        $response = [
            'success'   => true,
            'data'      => $user,
            'token'     => $token
        ];
    
        return response($response, 201);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        
        dd(Hash::make('admin'));

        auth()->logout();
        return response()->json([
            'success'    => true
        ], 200);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
