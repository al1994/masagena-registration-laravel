<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Services\SendMail;

class RegistrationController extends Controller
{
    private $mailService;

    public function __construct(SendMail $mailService) {
        $this->mailService = $mailService;
    }

    public function confirmation(Request $request) {

        try {

            $this->mailService->send();
            
            return response()->json([
                'success' => true,
                'message' => "Good Job"
            ], 200);

        } catch (\Throwable $th) {

            return response()->json([
                'success' => 'Throwable',
                'message' => $th->getMessage(),
                'line' => $th->getLine()
            ], 500);

        }
    }
}
