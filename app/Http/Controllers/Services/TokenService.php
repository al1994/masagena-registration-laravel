<?php

namespace App\Http\Controllers\Services;

use App\Models\User;

class TokenService {
    
    public function refreshToken($request) {

        $user = User::where('id', auth('sanctum')->user()->id)->first();

        $request->user()->currentAccessToken()->delete();
        $token = $user->createToken('ApiToken')->plainTextToken;

        return $token;
    }
}