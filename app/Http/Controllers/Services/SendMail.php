<?php
 
namespace App\Http\Controllers\Services;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MyEmail;
 
class SendMail {

    public function send()
    {
        $email = 'aalkhaer07@gmail.com';
        $data = [
            'title' => 'Selamat datang!',
            'url' => 'https://aantamim.id',
        ];
        Mail::to($email)->send(new MyEmail($data));
        return 'Berhasil mengirim email!';
    }
}