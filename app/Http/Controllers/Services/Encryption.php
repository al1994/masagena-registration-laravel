<?php

namespace App\Http\Controllers\Services;

class Encryption {
    
    private $dataKey;
    private $dataVektor;

    public function __construct() {
        $this->dataKey = config('const.path.dataKey');
        $this->dataVektor = config('const.path.dataVektor');
    }
    
    function myCrypt($value){
        $encrypted_data = openssl_encrypt($value, 'aes-256-cbc', $this->dataKey, OPENSSL_RAW_DATA, $this->dataVektor);
        return base64_encode($encrypted_data);
    }
    
    function myDecrypt($value){
        $value = base64_decode($value);
        $data = openssl_decrypt($value, 'aes-256-cbc', $this->dataKey, OPENSSL_RAW_DATA, $this->dataVektor);
        return $data;
    }
}