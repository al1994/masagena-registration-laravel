<?php

namespace App\Http\Controllers\Services;

use App\Models\User;
use App\Models\OptionsProfile;

class RegNumberGenerate {
    
    function generate(){

        $order = User::count();
        $order = $order+1;
        $options = OptionsProfile::first();
        $order = str_pad($order, 3, "0", STR_PAD_LEFT);

        $number = $options->registrationTag[$options->registrationTagActive]['number'];
        //001-ME0123 (urutan-ME Gelombang Tahun)
        $res['number'] = $order.'-ME'.$number;
        $res['tag'] = $options->registrationTagActive;

        return $res;
    }
}