<?php

namespace App\Http\Controllers\Services;

use Illuminate\Support\Facades\Storage;

class ImageService {

    public function __construct() {
        $this->path = config('const.path.registrationImage');
    }
    
    public function handleUploadedImage($image, $name)
    {
        $username = auth('sanctum')->user()->username;
        $filePath = $this->path.$username.'/';
        $fileName = $name.'.jpg';

        if(Storage::disk('local')->exists($filePath.$fileName)){
            Storage::delete($filePath.$fileName);
        }

        $path = Storage::putFileAs(
            $filePath, $image, $fileName
        );
        
        if(Storage::disk('local')->exists($path)){
            return true;
        }
        return false;
    }
}