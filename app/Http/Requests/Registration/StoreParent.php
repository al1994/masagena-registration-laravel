<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class StoreParent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'fatherName' => 'required|string|max:255',
            'motherName' => 'required|string|max:255', 
            'fatherIdentity' => 'required_without:motherIdentity|image|mimes:jpg,png,jpeg|max:1024',
            'motherIdentity' => 'required_without:fatherIdentity|image|mimes:jpg,png,jpeg|max:1024', 
        ];
    }
}
