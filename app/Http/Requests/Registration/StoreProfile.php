<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'birthPlace' => 'required|string|max:255',
            'birthDate' => 'required|string|max:255',
            'gender' => 'required|in:male,female|max:255',
            'maritalStatus' => 'required|in:0,1|max:255',
            'citizen' => 'required|string|max:255',
            // 'nationality' => 'required|string|max:255',
            'passportNumber' => 'required|string|max:255',
            // 'photo' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'regency' => 'required|string|max:255',
            'province' => 'required|string|max:255',
            'posCode' => 'required|string|max:255|regex:/^[A-Z0-9 ]+$/', 
            'phone' => 'required|string|max:255',
            'whatsapp' => 'required|string|max:255',
            'instagram' => 'string|max:255|regex:/^[A-Za-z0-9_]+$/',            
        ];
    }
}
