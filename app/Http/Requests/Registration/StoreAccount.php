<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class StoreAccount extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'typeRegistration' => 'required|string|in:Reguler,Fully Funded,Partially Funded',
            'destination'  => 'required|string|max:255',
            'level' => 'required|string|in:s1,s2,s3',
            'receipt' => 'required|image|mimes:jpg,png,jpeg|max:1024',
        ];
    }
}
