<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Websites\UniversityController;
use App\Http\Controllers\Websites\WebSettingsController;

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Admin\RegistrationController;
use App\Http\Controllers\Registration\UserController;
use App\Http\Controllers\Registration\RegistrationProfileController;
use App\Http\Controllers\Registration\ParentController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/v1/user', function (Request $request) {
    return $request->user();
});

Route::get('v1/activate/{id}/{username}', [UserController::class, 'activate']);
Route::get('v1/confirmation/', [RegistrationController::class, 'confirmation']);


Route::group(['prefix' => 'auth', 'middleware' => 'auth:sanctum'], function() {
    // manggil controller sesuai bawaan laravel 8
    Route::post('logout', [AuthController::class, 'logout']);
    // manggil controller dengan mengubah namespace di RouteServiceProvider.php biar bisa kayak versi2 sebelumnya
    Route::post('logoutall', 'AuthController@logoutall');
});

Route::group(['prefix' => 'v1'], function () use ($router) {
    $router->post('/register', [AuthController::class, 'register']);
    $router->post('/login', [AuthController::class, 'login']);

    $router->get('/university', [UniversityController::class, 'index']);
    // $router->post('/university', [UniversityController::class, 'store']);

}); 

Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum'], function () use ($router) {
    $router->get('/logout', [AuthController::class, 'logout']);
    $router->get('/web-settings', [WebSettingsController::class, 'setting']);

    // $router->get('/user', [UserController::class, 'index']);
    $router->post('/user',  [UserController::class, 'store']);

    $router->get('/profile', [RegistrationProfileController::class, 'index']);
    $router->post('/profile', [RegistrationProfileController::class, 'store']);
    $router->get('/parent', [ParentController::class, 'index']);
    $router->post('/parent', [ParentController::class, 'store']);
});
