<?php

return [

    'path' => [
        'registrationImage' => env('REGISTRATION_IMG_PATH'),
        'dataKey' => env('DATA_KEY'),
        'dataVektor' => env('DATA_VEKTOR'),
    ]
];
