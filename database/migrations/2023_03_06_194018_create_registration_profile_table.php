<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_profile', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('userId')->unique();
            $table->string('name')->nullable();
            $table->string('birthPlace')->nullable();
            $table->string('birthDate', 10)->nullable();
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->enum('maritalStatus', [0, 1])->default(0);
            $table->string('citizen')->nullable();
            $table->string('passportNumber')->nullable();
            $table->string('address')->nullable();
            $table->string('regency')->nullable();
            $table->string('province')->nullable();
            $table->string('posCode')->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('whatsapp', 15)->nullable();
            $table->string('fatherName')->nullable();
            $table->string('motherName')->nullable();
            $table->string('tertiaryEducation')->nullable();
            $table->string('institutionName')->nullable();
            $table->string('institutionCountry')->nullable();
            $table->string('graduationDate')->nullable();
            $table->json('graduationScore')->nullable();
            $table->enum('statusResume', [0, 1])->default(0);
            $table->enum('statusReceipt', [0, 1])->default(0);
            $table->enum('statusScholarship', [0, 1]);
            $table->enum('statusTypeScholarship', ['FullyFunded', 'PartiallyFunded', 'Reguler'])->default('Reguler');
            $table->enum('statusConfirmRegistration', [0, 1])->default(0);
            $table->enum('statusConfirmFile', [0, 1])->default(0);
            $table->json('universityListRegister')->nullable();
            $table->json('universityListPass')->nullable();
            $table->json('file')->nullable();
            $table->timestamps();

            $table->foreign('userId')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_profile');
    }
};
