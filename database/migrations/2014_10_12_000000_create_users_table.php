<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('registrationNumber', 10)->unique();
            $table->string('registrationTag', 20);
            $table->string('username')->unique();
            $table->string('destination')->nullable();
            $table->enum('level', ['s1', 's2', 's3'])->default('s1');
            $table->enum('typeRegistration', ['Reguler', 'Fully Funded', 'Partially Funded'])->default('Reguler');
            $table->enum('accountStatus', ['register', 'waiting', 'active', 'finish', 'pass', 'blocked'])->default('register');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('google_id')->nullable();
            $table->timestamps();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
