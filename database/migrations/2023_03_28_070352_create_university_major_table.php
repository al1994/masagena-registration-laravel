<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_major', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('university_id');
            $table->enum('level', ['s1', 's2', 's3'])->default('s1');
            $table->string('slugFaculty');
            $table->string('faculty');
            $table->string('slugMajor');
            $table->string('major');
            $table->string('spp');
            $table->string('author');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('isDelete', ["true", "false"])->default("true");
            $table->timestamps();
            $table->foreign('university_id')
                    ->references('id')
                    ->on('university')
                    ->onCascade('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_major');
    }
};
