<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('slugId')->unique();
            $table->string('name');
            $table->string('country');
            $table->string('province');
            $table->json('faculty')->nullable();
            $table->json('majors')->nullable();
            $table->text('description')->nullable();
            $table->string('sites')->nullable();
            $table->string('image');
            $table->string('author');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('isDelete', ["true", "false"])->default("true");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university');
    }
};
